package com.sda.graduationProject.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sda.graduationProject.entity.Assignment;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@JsonInclude(value= JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto {

    private Integer userId;
    private Integer userPersonId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private Map<Integer, String> assignments = new TreeMap<>();

    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserPersonId() {
        return userPersonId;
    }
    public void setUserPersonId(Integer userId) {
        this.userPersonId = userPersonId;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Map<Integer, String> getAssignments() {
        return assignments;
    }
    public void setAssignments(List<Assignment> assignments) {
        Map<Integer, String> assignmentsMap = new TreeMap<>();
        for (Assignment assignment : assignments) {
            assignmentsMap.put(assignment.getAssignmentId(), assignment.getEmployeeName());
        }
        this.assignments = assignmentsMap;
    }

}
