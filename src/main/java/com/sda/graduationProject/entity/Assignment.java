package com.sda.graduationProject.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="assignment_id")
    private Integer assignmentId;
    @Column
    private String status;
    @Column
    private Integer progress;
    @Column
    private String importance;
    @Column
    private String description;
    @Column
    private Date creationDate;
    @Column
    private Date lunchDate;
    @Column
    private Date completionDate;
    @Column
    private String employeePersonId;
    @Column
    private String employeeName;
    @Column
    private String assignmentType;
    @Column
    private String assignmentDuration;
    @Column
    private String position;
    @Column
    private String newSupervisor;
    @Column
    private String homeCountry;
    @Column
    private String homeCity;
    @Column
    private String currentCity;
    @Column
    private String assignmentHostCountry;
    @Column
    private String assignmentHostCity;
    @Column
    private String workCity;
    @Column
    private Integer currentPca;
    @Column
    private Integer currentPcp;
    @Column
    private Integer newPca;
    @Column
    private Integer newPcp;
    @Column
    private Integer performance;
    @Column
    private String potential;
    @Column
    private String familyGroup;
    @Column
    private String familyGroupTravelling;
    @Column
    private Date startingDate;
    @Column
    private Date endingDate;
    @Column
    private String assignmentReason;
    @Column
    private String comments;

    @ManyToOne
    @JoinColumn(name = "user_id",nullable = false)
    private User user;

    public Integer getAssignmentId() {
        return assignmentId;
    }
    public void setAssignmentId(Integer assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getProgress() {
        return progress;
    }
    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public String getImportance() {
        return importance;
    }
    public void setImportance(String importance) {
        this.importance = importance;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLunchDate() {
        return lunchDate;
    }
    public void setLunchDate(Date lunchDate) {
        this.lunchDate = lunchDate;
    }

    public Date getCompletionDate() {
        return completionDate;
    }
    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getEmployeePersonId() {
        return employeePersonId;
    }
    public void setEmployeePersonId(String employeePersonId) {
        this.employeePersonId = employeePersonId;
    }

    public String getEmployeeName() {
        return employeeName;
    }
    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getAssignmentType() {
        return assignmentType;
    }
    public void setAssignmentType(String assignmentType) {
        this.assignmentType = assignmentType;
    }

    public String getAssignmentDuration() {
        return assignmentDuration;
    }
    public void setAssignmentDuration(String assignmentDuration) {
        this.assignmentDuration = assignmentDuration;
    }

    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }

    public String getNewSupervisor() {
        return newSupervisor;
    }
    public void setNewSupervisor(String newSupervisor) {
        this.newSupervisor = newSupervisor;
    }

    public String getHomeCountry() {
        return homeCountry;
    }
    public void setHomeCountry(String homeCountry) {
        this.homeCountry = homeCountry;
    }

    public String getHomeCity() {
        return homeCity;
    }
    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getCurrentCity() {
        return currentCity;
    }
    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    public String getAssignmentHostCountry() {
        return assignmentHostCountry;
    }
    public void setAssignmentHostCountry(String assignmentHostCountry) {
        this.assignmentHostCountry = assignmentHostCountry;
    }

    public String getAssignmentHostCity() {
        return assignmentHostCity;
    }
    public void setAssignmentHostCity(String assignmentHostCity) {
        this.assignmentHostCity = assignmentHostCity;
    }

    public String getWorkCity() {
        return workCity;
    }
    public void setWorkCity(String workCity) {
        this.workCity = workCity;
    }

    public Integer getCurrentPca() {
        return currentPca;
    }
    public void setCurrentPca(Integer currentPca) {
        this.currentPca = currentPca;
    }

    public Integer getCurrentPcp() {
        return currentPcp;
    }
    public void setCurrentPcp(Integer currentPcp) {
        this.currentPcp = currentPcp;
    }

    public Integer getNewPca() {
        return newPca;
    }
    public void setNewPca(Integer newPca) {
        this.newPca = newPca;
    }

    public Integer getNewPcp() {
        return newPcp;
    }
    public void setNewPcp(Integer newPcp) {
        this.newPcp = newPcp;
    }

    public Integer getPerformance() {
        return performance;
    }
    public void setPerformance(Integer performance) {
        this.performance = performance;
    }

    public String getPotential() {
        return potential;
    }
    public void setPotential(String potential) {
        this.potential = potential;
    }

    public String getFamilyGroup() {
        return familyGroup;
    }
    public void setFamilyGroup(String familyGroup) {
        this.familyGroup = familyGroup;
    }

    public String getFamilyGroupTravelling() {
        return familyGroupTravelling;
    }
    public void setFamilyGroupTravelling(String familyGroupTravelling) {
        this.familyGroupTravelling = familyGroupTravelling;
    }

    public Date getStartingDate() {
        return startingDate;
    }
    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public Date getEndingDate() {
        return endingDate;
    }
    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public String getAssignmentReason() {
        return assignmentReason;
    }
    public void setAssignmentReason(String assignmentReason) {
        this.assignmentReason = assignmentReason;
    }

    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

}
