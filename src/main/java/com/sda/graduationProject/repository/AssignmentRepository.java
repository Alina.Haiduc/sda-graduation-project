package com.sda.graduationProject.repository;

import com.sda.graduationProject.entity.Assignment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssignmentRepository extends PagingAndSortingRepository<Assignment, Integer> {

    List<Assignment> findAllByStatus(String status);

    //Example method with direct Query. a is like alias
    //@Query("select a from Assignment a where a.user.userId=1")
    //List<Assignment> getAssignmentsByUserIdWithQuery();

}
