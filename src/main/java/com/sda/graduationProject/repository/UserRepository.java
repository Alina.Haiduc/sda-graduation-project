package com.sda.graduationProject.repository;

import com.sda.graduationProject.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

    Optional<User> findByUserPersonId(Integer userPersonId);
    List<User> findAllByFirstName(String firstName);
    List<User> findAllByLastName(String lastName);

}
