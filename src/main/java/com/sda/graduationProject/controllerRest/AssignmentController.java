package com.sda.graduationProject.controllerRest;

import com.sda.graduationProject.components.CustomFaker;
import com.sda.graduationProject.dto.AssignmentDto;
import com.sda.graduationProject.entity.Assignment;
import com.sda.graduationProject.service.AssignmentService;
import com.sda.graduationProject.service.mapper.AssignmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/v1/assignments")
public class AssignmentController {

    private final AssignmentService assignmentService;
    private final AssignmentMapper assignmentMapper;

    @Autowired
    public AssignmentController(AssignmentService assignmentService,
                                AssignmentMapper assignmentMapper) {
        this.assignmentService = assignmentService;
        this.assignmentMapper = assignmentMapper;
    }

    @Autowired
    private CustomFaker customFaker;

    @PostMapping("/populate")
    public void faker() {
        assignmentService.createAll(customFaker.createDummyAssignmentsList());
    }

    /*This will not work now because the user column is set up with not null.
    Because the idea is to don't be allowed to create an assignment that is not assigned to an user.*/
    @PostMapping("/create")
    public ResponseEntity<AssignmentDto> create(@RequestBody Assignment assignment) {
        return ResponseEntity.ok(assignmentMapper.toAssignmentDto(assignmentService.create(assignment)));
    }

    @PostMapping("/createWithUser")
    public ResponseEntity<AssignmentDto> createWithUser(@RequestBody Assignment assignment,
                                                        @RequestParam Integer userId) {
        return ResponseEntity.ok(assignmentMapper.toAssignmentDtoWithUser(
                assignmentService.createWithUser((assignment), userId), userId));
    }

    @GetMapping("/getAllPaging")
    public ResponseEntity<List<AssignmentDto>> getAllPaging(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer itemsOnPage,
            @RequestParam(defaultValue = "id") String sortBy) {
        List<AssignmentDto> assignmentsDto = assignmentService.getAllPaging(pageNo, itemsOnPage, sortBy);
        if (assignmentsDto.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(assignmentsDto);
        }
    }

    @GetMapping("/getAllDto")
    public ResponseEntity<List<AssignmentDto>> getAllDto() {
        List<AssignmentDto> assignmentsDto = assignmentService.getAllDto();
        if (assignmentsDto.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(assignmentsDto);
        }
    }

    @GetMapping("/getAllAssignmentsByUserId/{id}")
    public ResponseEntity<List<AssignmentDto>> getAllAssignmentsByUserId(@PathVariable Integer id) {
        List<AssignmentDto> assignmentsDto = assignmentService.getAllAssignmentsByUserId(id);
        if (assignmentsDto != null) {
            return ResponseEntity.ok(assignmentsDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("/getDtoById/{id}")
    public ResponseEntity<AssignmentDto> getDtoById(@PathVariable Integer id) {
        return ResponseEntity.ok(assignmentService.getDtoById(id));
    }

    @GetMapping("/getAllByStatus")
    public ResponseEntity<List<AssignmentDto>> getAllByStatus(@RequestParam("status") String status) {
        List<AssignmentDto> assignmentsDto = assignmentService.getAllByStatus(status);
        if (assignmentsDto.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(assignmentsDto);
        }
    }

    @GetMapping("/getAllAssignmentsByUserPersonId/{userPersonId}")
    public ResponseEntity<List<AssignmentDto>> getAllAssignmentsByUserPersonId(@PathVariable Integer userPersonId) {
        List<AssignmentDto> assignmentsDto = assignmentService.getAllAssignmentsByUserPersonId(userPersonId);
        if (assignmentsDto != null) {
            return ResponseEntity.ok(assignmentsDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @PutMapping("/updateStatus/{id},{status}")
    public ResponseEntity<AssignmentDto> updateStatus(@PathVariable Integer id, @PathVariable String status) {
        return ResponseEntity.ok(assignmentMapper.toAssignmentDto(assignmentService.updateStatus(id, status)));
    }

    @PutMapping("/update")
    public ResponseEntity<AssignmentDto> update(@RequestBody Assignment assignment,
                                                @RequestParam Integer userPersonId) {
        AssignmentDto assignmentDto = assignmentMapper.toAssignmentDtoWithUserPersonId
                (assignmentService.update(assignment, userPersonId), userPersonId);
        if (assignmentDto != null) {
            return ResponseEntity.ok(assignmentDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable Integer id) {
        Assignment assignment = assignmentService.getAssignmentById(id);
        if (assignment != null) {
            assignmentService.delete(id);
            return "The assignment with the id " + id + " has been deleted";
        } else {
            return "The assignment with the id " + id + " does't exist";
        }
    }

}
