package com.sda.graduationProject.controllerRest;

import com.sda.graduationProject.components.CustomFaker;
import com.sda.graduationProject.dto.AssignmentDto;
import com.sda.graduationProject.dto.UserDto;
import com.sda.graduationProject.entity.User;
import com.sda.graduationProject.service.AssignmentService;
import com.sda.graduationProject.service.UserService;
import com.sda.graduationProject.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final AssignmentService assignmentService;

    @Autowired
    public UserController(UserService userService,
                          UserMapper userMapper,
                          AssignmentService assignmentService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.assignmentService = assignmentService;
    }

    @Autowired
    private CustomFaker customFaker;

    @PostMapping("/populate")
    public void faker() {
        userService.createAll(customFaker.createDummyUsersList());
    }

    @PostMapping("/create")
    public ResponseEntity<User> create(@RequestBody User user) {
        return ResponseEntity.ok(userService.create(user));
    }

    @GetMapping("/getAllPaging")
    public ResponseEntity<List<UserDto>> getAllPaging(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer itemsOnPage,
            @RequestParam(defaultValue = "id") String sortBy) {
        List<UserDto> usersDto = userService.getAllPaging(pageNo, itemsOnPage, sortBy);
        if (usersDto.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(usersDto);
        }
    }

    @GetMapping("/getAllDto")
    public ResponseEntity<List<UserDto>> getAllDto() {
        List<UserDto> usersDto = userService.getAllDto();
        if (usersDto.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(usersDto);
        }
    }

    @GetMapping("/getUserDtoById/{id}")
    public ResponseEntity<UserDto> getUserDtoById(@PathVariable Integer id) {
        UserDto userDto = userService.getUserDtoById(id);
        if (userDto != null) {
            return ResponseEntity.ok(userDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("/getByPersonId/{id}")
    public ResponseEntity<UserDto> getByPersonId(@PathVariable Integer id) {
        UserDto userDto = userService.getByPersonId(id);
        if (userDto != null) {
            return ResponseEntity.ok(userDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("getAllByFirstName")
    public ResponseEntity<List<UserDto>> getAllByFirstName(@RequestParam("firstName") String firstName) {
        List<UserDto> usersDto = userService.getAllByFirstName(firstName);
        if (usersDto != null) {
            return ResponseEntity.ok(usersDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("getAllByLastName")
    public ResponseEntity<List<UserDto>> getAllByLastName(@RequestParam("lastName") String lastName) {
        List<UserDto> usersDto = userService.getAllByLastName(lastName);
        if (usersDto != null) {
            return ResponseEntity.ok(usersDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("getAllByFullName")
    public ResponseEntity<List<UserDto>> getAllByFullName(@RequestParam("firstName") String firstName,
                                                          @RequestParam("lastName") String lastName) {
        List<UserDto> usersDto = userService.getAllByFullName(firstName, lastName);
        if (usersDto != null) {
            return ResponseEntity.ok(usersDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    //Working, but to include also check/exception handler if the person Id already exist for another user
    @PutMapping("update")
    public ResponseEntity<UserDto> update(@RequestBody User user) {
        UserDto userDto = userMapper.toUserDto(userService.update(user));
        if (userDto != null) {
            return ResponseEntity.ok(userDto);
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable Integer id) {
        User user = userService.getUserById(id);
        List<AssignmentDto> assignmentsDto = assignmentService.getAllAssignmentsByUserId(id);
        if (user != null) {
            if (assignmentsDto.isEmpty()) {
                userService.delete(id);
                return "The employee with the id " + id + " has been deleted";
            } else {
                return "The employee with the id " + id + " has assignments assigned and can't be deleted";
            }
        } else {
            return "The employee with the id " + id + " does't exist";
        }
    }
}
