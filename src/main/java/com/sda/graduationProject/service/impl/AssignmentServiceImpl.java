package com.sda.graduationProject.service.impl;

import com.sda.graduationProject.dto.AssignmentDto;
import com.sda.graduationProject.entity.Assignment;
import com.sda.graduationProject.entity.User;
import com.sda.graduationProject.repository.AssignmentRepository;
import com.sda.graduationProject.repository.UserRepository;
import com.sda.graduationProject.service.AssignmentService;
import com.sda.graduationProject.service.UserService;
import com.sda.graduationProject.service.mapper.AssignmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private final AssignmentRepository assignmentRepository;
    private final AssignmentMapper assignmentMapper;
    private final UserService userService;

    @Autowired
    private AssignmentServiceImpl(AssignmentRepository assignmentRepository,
                                  AssignmentMapper assignmentMapper,
                                  UserService userService) {
        this.assignmentRepository = assignmentRepository;
        this.assignmentMapper = assignmentMapper;
        this.userService = userService;
    }

    @Override
    public void createAll(List<Assignment> assignments) {
        assignmentRepository.saveAll(assignments);
    }

    @Override
    public Assignment create(Assignment assignment) {
        return assignmentRepository.save(assignment);
    }

    @Override
    public Assignment createWithUser(Assignment assignment, Integer userId) {
        User user = userService.getUserById(userId);
        assignment.setUser(user);
        return assignmentRepository.save(assignment);
    }


    @Override
    public List<AssignmentDto> getAllPaging(Integer pageNo, Integer itemsOnPage, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, itemsOnPage, Sort.by(sortBy));
        Page<Assignment> assignments = assignmentRepository.findAll(paging);
        List<Assignment> assignmentList = assignments.getContent();
        List<AssignmentDto> assignmentDtoList = new ArrayList<>();
        for (Assignment assignment : assignmentList) {
            assignmentDtoList.add(assignmentMapper.toAssignmentDto(assignment));
        }
        return assignmentDtoList;
    }

    @Override
    public List<AssignmentDto> getAllDto() {
        Iterable<Assignment> assignments = assignmentRepository.findAll();
        List<AssignmentDto> assignmentsDto = new ArrayList<>();
        for (Assignment assignment : assignments) {
            assignmentsDto.add(assignmentMapper.toAssignmentDto(assignment));
        }
        return assignmentsDto;
    }

    @Override
    public List<AssignmentDto> getAllAssignmentsByUserId(Integer userId) {
        Iterable<Assignment> assignments = assignmentRepository.findAll();
        List<AssignmentDto> assignmentsDto = new ArrayList<>();
        for (Assignment assignment : assignments) {
            int assignmentReceivedId = assignment.getUser().getUserId();
            if (assignmentReceivedId == userId) {
                assignmentsDto.add(assignmentMapper.toAssignmentDtoWithUser(assignment, assignment.getUser().getUserId()));
            }
        }
        return assignmentsDto;
    }

    @Override
    public AssignmentDto getDtoById(Integer id) {
        Optional<Assignment> assignment = assignmentRepository.findById(id);
        return assignmentMapper.toAssignmentDto(assignment.get());
    }

    @Override
    public Assignment getAssignmentById(Integer id) {
        try {
            return assignmentRepository.findById(id).get();
        } catch (NoSuchElementException n) {
            return null;
        }
    }

    @Override
    public List<AssignmentDto> getAllByStatus(String status) {
        List<Assignment> assignmentsList = assignmentRepository.findAllByStatus(status);
        List<AssignmentDto> assignmentsDtoList = new ArrayList<>();
        for (Assignment assignment : assignmentsList) {
            assignmentsDtoList.add(assignmentMapper.toAssignmentDto(assignment));
        }
        return assignmentsDtoList;
    }

    @Override
    public List<AssignmentDto> getAllAssignmentsByUserPersonId(Integer userPersonId) {
        Iterable<Assignment> assignments = assignmentRepository.findAll();
        List<AssignmentDto> assignmentsDto = new ArrayList<>();
        for (Assignment assignment : assignments) {
            int assignmentReceivedId = assignment.getUser().getUserPersonId();
            if (assignmentReceivedId == userPersonId) {
                assignmentsDto.add(assignmentMapper.toAssignmentDtoWithUser(assignment, assignment.getUser().getUserPersonId()));
            }
        }
        return assignmentsDto;
    }

    @Override
    public Assignment updateStatus(Integer id, String status) {
        Assignment assignment = assignmentRepository.findById(id).get();
        assignment.setStatus(status);
        return assignmentRepository.save(assignment);
    }

    @Override
    public Assignment update(Assignment assignment, Integer userPersonId) {
        try {
            Assignment assignmentToUpdate = getAssignmentById(assignment.getAssignmentId());
            assignmentToUpdate.setAssignmentId(assignment.getAssignmentId());
            assignmentToUpdate.setStatus(assignment.getStatus());
            assignmentToUpdate.setProgress(assignment.getProgress());
            assignmentToUpdate.setImportance(assignment.getImportance());
            assignmentToUpdate.setDescription(assignment.getDescription());
            assignmentToUpdate.setCreationDate(assignment.getCreationDate());
            assignmentToUpdate.setLunchDate(assignment.getLunchDate());
            assignmentToUpdate.setCompletionDate(assignment.getCompletionDate());
            assignmentToUpdate.setEmployeePersonId(assignment.getEmployeePersonId());
            assignmentToUpdate.setEmployeeName(assignment.getEmployeeName());
            assignmentToUpdate.setAssignmentType(assignment.getAssignmentType());
            assignmentToUpdate.setAssignmentDuration(assignment.getAssignmentDuration());
            assignmentToUpdate.setPosition(assignment.getPosition());
            assignmentToUpdate.setNewSupervisor(assignment.getNewSupervisor());
            assignmentToUpdate.setHomeCountry(assignment.getHomeCountry());
            assignmentToUpdate.setHomeCity(assignment.getHomeCity());
            assignmentToUpdate.setCurrentCity(assignment.getCurrentCity());
            assignmentToUpdate.setAssignmentHostCountry(assignment.getAssignmentHostCountry());
            assignmentToUpdate.setAssignmentHostCity(assignment.getAssignmentHostCity());
            assignmentToUpdate.setWorkCity(assignment.getWorkCity());
            assignmentToUpdate.setCurrentPca(assignment.getCurrentPca());
            assignmentToUpdate.setCurrentPcp(assignment.getCurrentPcp());
            assignmentToUpdate.setNewPca(assignment.getNewPca());
            assignmentToUpdate.setNewPcp(assignment.getNewPcp());
            assignmentToUpdate.setPerformance(assignment.getPerformance());
            assignmentToUpdate.setPotential(assignment.getPotential());
            assignmentToUpdate.setFamilyGroup(assignment.getFamilyGroup());
            assignmentToUpdate.setFamilyGroupTravelling(assignment.getFamilyGroupTravelling());
            assignmentToUpdate.setStartingDate(assignment.getStartingDate());
            assignmentToUpdate.setEndingDate(assignment.getEndingDate());
            assignmentToUpdate.setAssignmentReason(assignment.getAssignmentReason());
            assignmentToUpdate.setComments(assignment.getComments());

            assignmentToUpdate.setUser(userService.getUserByPersonId(userPersonId));

            return assignmentRepository.save(assignmentToUpdate);
        } catch (NoSuchElementException n) {
            return null;
        }
    }

    @Override
    public void delete(Integer id) {
        assignmentRepository.deleteById(id);
    }
}
