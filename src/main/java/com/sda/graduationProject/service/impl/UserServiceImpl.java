package com.sda.graduationProject.service.impl;

import com.sda.graduationProject.dto.UserDto;
import com.sda.graduationProject.entity.User;
import com.sda.graduationProject.repository.UserRepository;
import com.sda.graduationProject.service.UserService;
import com.sda.graduationProject.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    private UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public void createAll(List<User> users) {
        userRepository.saveAll(users);
    }

    @Override
    public User create(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<UserDto> getAllPaging(Integer pageNo, Integer itemsOnPage, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, itemsOnPage, Sort.by(sortBy));
        Page<User> users = userRepository.findAll(paging);
        List<User> usersList = users.getContent();
        List<UserDto> usersDtoList = new ArrayList<>();
        for (User user : usersList) {
            usersDtoList.add(userMapper.toUserDto(user));
        }
        return usersDtoList;
    }

    @Override
    public List<UserDto> getAllDto() {
        Iterable<User> usersList = userRepository.findAll();
        List<UserDto> usersDtoList = new ArrayList<>();
        for (User user : usersList) {
            usersDtoList.add(userMapper.toUserDto(user));
        }
        return usersDtoList;
    }

    @Override
    public Iterable<User> getAllUsers() {
        Iterable<User> usersList = userRepository.findAll();
        try {
            return usersList;
        } catch (NoSuchElementException n) {
            return null;
        }
    }

    @Override
    public UserDto getUserDtoById(Integer id) {
        Optional<User> user;
        try {
            user = userRepository.findById(id);
            return userMapper.toUserDto(user.get());
        } catch (NoSuchElementException n) {
            return null;
        }
    }

    @Override
    public User getUserById(Integer id) {
        try {
            return userRepository.findById(id).get();
        } catch (NoSuchElementException n) {
            return null;
        }
    }

    @Override
    public UserDto getByPersonId(Integer id) {
        Optional<User> user;
        try {
            user = userRepository.findByUserPersonId(id);
            return userMapper.toUserDto(user.get());
        } catch (NoSuchElementException n) {
            return null;
        }
    }

    @Override
    public List<UserDto> getAllByFirstName(String firstName) {
        List<User> usersList = userRepository.findAllByFirstName(firstName);
        List<UserDto> usersDtoList = new ArrayList<>();
        for (User user : usersList) {
            usersDtoList.add(userMapper.toUserDto(user));
        }
        return usersDtoList;
    }

    @Override
    public List<UserDto> getAllByLastName(String lastName) {
        List<User> usersList = userRepository.findAllByLastName(lastName);
        List<UserDto> usersDtoList = new ArrayList<>();
        for (User user : usersList) {
            usersDtoList.add(userMapper.toUserDto(user));
        }
        return usersDtoList;
    }

    @Override
    public List<UserDto> getAllByFullName(String firstName, String lastName) {
        List<User> usersWithSameLastName = userRepository.findAllByLastName(lastName);
        List<User> usersWithSameName = new ArrayList<>();
        for (User user : usersWithSameLastName) {
            if (user.getFirstName().equals(firstName)) {
                usersWithSameName.add(user);
            }
        }
        List<UserDto> usersDtoList = new ArrayList<>();
        for (User user : usersWithSameName) {
            usersDtoList.add(userMapper.toUserDto(user));
        }
        return usersDtoList;
    }

    @Override
    public User getUser(Integer id) {
        return userRepository.findById(id).get();
    }

    @Override
    public User getUserByPersonId(Integer userPersonId) {
        return userRepository.findByUserPersonId(userPersonId).get();
    }

    @Override
    public User update(User user) {
        try {
            User userToUpdate = getUserById(user.getUserId());
            userToUpdate.setUserPersonId(user.getUserPersonId());
            userToUpdate.setFirstName(user.getFirstName());
            userToUpdate.setLastName(user.getLastName());
            userToUpdate.setFunction(user.getFunction());
            userToUpdate.setEmailAddress(user.getEmailAddress());
            userToUpdate.setPassword(user.getPassword());
            return userRepository.save(userToUpdate);
        } catch (NoSuchElementException n) {
            return null;
        }
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }
}
