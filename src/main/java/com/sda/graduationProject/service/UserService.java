package com.sda.graduationProject.service;

import com.sda.graduationProject.dto.UserDto;
import com.sda.graduationProject.entity.User;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

@Service
public interface UserService {

    void createAll(List<User> usersList);
    User create(User user);
    List<UserDto> getAllPaging(Integer pageNo, Integer itemsOnPage, String sortBy);
    List<UserDto> getAllDto();
    Iterable<User> getAllUsers();
    UserDto getUserDtoById(Integer id);
    User getUserById(Integer id);
    UserDto getByPersonId(Integer id);
    List<UserDto> getAllByFirstName(String firstName);
    List<UserDto> getAllByLastName(String lastName);
    List<UserDto> getAllByFullName(String firstName, String lastName);
    User getUser(Integer id);
    User getUserByPersonId(Integer userPersonId);
    User update(User user);
    void delete(Integer id);
}
