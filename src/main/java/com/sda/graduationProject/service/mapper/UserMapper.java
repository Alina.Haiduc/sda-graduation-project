package com.sda.graduationProject.service.mapper;

import com.sda.graduationProject.dto.UserDto;
import com.sda.graduationProject.entity.User;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {

    public UserDto toUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUserId(user.getUserId());
        userDto.setUserPersonId(user.getUserPersonId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmailAddress(user.getEmailAddress());
        userDto.setAssignments(user.getAssignments());
        return userDto;
    }
}
