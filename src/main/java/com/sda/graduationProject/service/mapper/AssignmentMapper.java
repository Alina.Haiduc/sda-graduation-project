package com.sda.graduationProject.service.mapper;

import com.sda.graduationProject.dto.AssignmentDto;
import com.sda.graduationProject.entity.Assignment;
import com.sda.graduationProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AssignmentMapper {

    private final UserService userService;

    @Autowired
    public AssignmentMapper(UserService userService) {
        this.userService = userService;
    }

    public AssignmentDto toAssignmentDto(Assignment assignment) {
        AssignmentDto assignmentDto = new AssignmentDto();
        assignmentDto.setAssignmentId(assignment.getAssignmentId());
        assignmentDto.setEmployeeName(assignment.getEmployeeName());
        assignmentDto.setStatus(assignment.getStatus());
        assignmentDto.setImportance(assignment.getImportance());
        return assignmentDto;
    }

    public AssignmentDto toAssignmentDtoWithUser(Assignment assignment, Integer userId) {
        AssignmentDto assignmentDto = new AssignmentDto();
        assignmentDto.setAssignmentId(assignment.getAssignmentId());
        assignmentDto.setEmployeeName(assignment.getEmployeeName());
        assignmentDto.setStatus(assignment.getStatus());
        assignmentDto.setImportance(assignment.getImportance());
        assignmentDto.setUser(userId);
        return assignmentDto;
    }

    public AssignmentDto toAssignmentDtoWithUserPersonId(Assignment assignment, Integer userPersonId) {
        AssignmentDto assignmentDto = new AssignmentDto();
        assignmentDto.setAssignmentId(assignment.getAssignmentId());
        assignmentDto.setEmployeeName(assignment.getEmployeeName());
        assignmentDto.setStatus(assignment.getStatus());
        assignmentDto.setImportance(assignment.getImportance());
        assignmentDto.setUser(userService.getUserByPersonId(userPersonId).getUserId());
        return assignmentDto;
    }

}
