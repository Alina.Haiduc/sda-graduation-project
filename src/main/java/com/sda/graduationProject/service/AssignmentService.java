package com.sda.graduationProject.service;

import com.sda.graduationProject.dto.AssignmentDto;
import com.sda.graduationProject.entity.Assignment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AssignmentService {

    void createAll(List<Assignment> assignments);
    Assignment create(Assignment assignment);
    Assignment createWithUser(Assignment assignment,Integer userId);
    List<AssignmentDto> getAllPaging(Integer pageNo, Integer itemsOnPage, String sortBy);
    List<AssignmentDto> getAllDto();
    AssignmentDto getDtoById(Integer id);
    Assignment getAssignmentById(Integer id);
    List<AssignmentDto> getAllAssignmentsByUserId(Integer userId);
    List<AssignmentDto> getAllByStatus(String status);
    List<AssignmentDto> getAllAssignmentsByUserPersonId(Integer userPersonId);
    Assignment updateStatus(Integer id, String status);
    Assignment update(Assignment assignment,Integer userPersonId);
    void delete(Integer id);
}
