package com.sda.graduationProject.components;

import com.github.javafaker.Faker;
import com.sda.graduationProject.entity.Assignment;
import com.sda.graduationProject.entity.User;
import com.sda.graduationProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class CustomFaker {

    Faker faker;

    private final UserService userService;

    @Autowired
    public CustomFaker(Faker faker, UserService userService) {
        this.faker = faker;
        this.userService = userService;
    }

    public List<User> createDummyUsersList() {
        List<User> dummyUsers = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setUserPersonId((int) faker.number().randomNumber(8, true));
            user.setFirstName(faker.name().firstName());
            user.setLastName(faker.name().lastName());
            user.setFunction(faker.job().position());
            user.setEmailAddress(faker.internet().emailAddress());
            user.setPassword(faker.internet().password());
            dummyUsers.add(user);
        }
        return dummyUsers;
    }

    public List<Assignment> createDummyAssignmentsList() {
        List<Assignment> dummyAssignments = new ArrayList<>();
        Iterable<User> users = userService.getAllUsers();
        List<Integer> userIdsList = new ArrayList<>();
        if (users != null) {
            for (User user : users) {
                userIdsList.add(user.getUserId());
            }
        } else {
            userIdsList.add(0);
        }
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            Assignment assignment = new Assignment();
            assignment.setStatus(faker.options().option("approved", "pending approval", "rejected"));
            assignment.setProgress(Integer.valueOf(faker.options().option("25", "50", "75", "100")));
            assignment.setImportance(faker.options().option("High", "Medium", "Low"));
            assignment.setDescription(faker.options().option("description1", "description2"));
            assignment.setEmployeeName(faker.name().name());
            int randomUserId = userIdsList.get(random.nextInt(userIdsList.size()));
            User user = userService.getUserById(randomUserId);
            assignment.setUser(user);
            dummyAssignments.add(assignment);
        }
        return dummyAssignments;
    }

}
